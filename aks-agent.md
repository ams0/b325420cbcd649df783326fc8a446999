
```bash
#https://github.com/Hyperfish/vsts-build.git

# values.yaml
vsts:
  enabled: true
  repository: microsoft/vsts-agent
  tag: "ubuntu-16.04-docker-18.06.1-ce-standard"
  replicaCount: 2
  pullPolicy: IfNotPresent
  nodeSelector: {}
  tolerations: []
  affinity: {}
  # env vars
  configMap:
    VSTS_ACCOUNT: ""
    VSTS_POOL: ""
  secret:
    VSTS_TOKEN: ""
    

 
helm upgrade --install --namespace vsts \
--set vsts.configMap.VSTS_ACCOUNT="cookingwithazure" \
--set vsts.configMap.VSTS_POOL="AKS" \
--set vsts.secret.VSTS_TOKEN="" \
--set vsts.tag="ubuntu-16.04-docker-18.06.1-ce-standard"
--reuse-values vsts-agents https://storeme.blob.core.windows.net/charts/vsts-build-0.1.0.tgz

```